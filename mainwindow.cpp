#include "mainwindow.h"
#include "inventorytable.h"

#include <QPushButton>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDesktopWidget>
#include <QPropertyAnimation>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowState(Qt::WindowMaximized);

    backgroundWidget = new QWidget(this);
    backgroundWidget->setFixedSize(qApp->desktop()->width(), qApp->desktop()->height());
    backgroundWidget->setStyleSheet("background-color:gray");
    setCentralWidget(backgroundWidget);

    inventoryWidget = new QWidget(backgroundWidget);
    inventoryWidget->setMinimumSize(backgroundWidget->size());
    inventoryWidget->setStyleSheet("background-color:white");
    inventoryWidget->setEnabled(false);

    buttonsWidget = new QWidget(backgroundWidget);
    buttonsWidget->setStyleSheet("background-color:lightyellow");
    buttonsWidget->setMinimumWidth(backgroundWidget->width());

    int menuButtonsWidth = qApp->desktop()->width()/3;

    startButton = new QPushButton(buttonsWidget);
    startButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
    startButton->setMinimumSize(menuButtonsWidth,startButton->minimumSizeHint().height());
    startButton->setText(tr("Начать"));
    connect(startButton, SIGNAL(clicked(bool)), this, SLOT(hideMainMenu()));

    stopButton = new QPushButton(buttonsWidget);
    stopButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
    stopButton->setMinimumSize(menuButtonsWidth,startButton->minimumSizeHint().height());
    stopButton->setText(tr("Выход"));
    connect(stopButton, SIGNAL(clicked(bool)), this, SLOT(onStopPressed()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(startButton);
    buttonsLayout->addWidget(stopButton);
    buttonsWidget->setLayout(buttonsLayout);
    mainMenuGeometry = buttonsWidget->geometry();
    mainMenuGeometry.setHeight(buttonsWidget->height() * 2);
    createInventoryWidgets();
    showMainMenu();
}

MainWindow::~MainWindow()
{

}

void MainWindow::onStopPressed()
{
    qApp->exit(0);
}

void MainWindow::showMainMenu()
{
    QPropertyAnimation *sizeAnimation = new QPropertyAnimation(buttonsWidget, "geometry", this);
    sizeAnimation->setDuration(1500);
    sizeAnimation->setStartValue(QRect());
    sizeAnimation->setEndValue(mainMenuGeometry);
    sizeAnimation->start();
    inventoryWidget->setEnabled(false);
}

void MainWindow::hideMainMenu()
{
    QPropertyAnimation *sizeAnimation = new QPropertyAnimation(buttonsWidget, "geometry", this);
    sizeAnimation->setDuration(1500);
    sizeAnimation->setStartValue(mainMenuGeometry);
    sizeAnimation->setEndValue(QRect());
    sizeAnimation->start();
    inventoryWidget->setEnabled(true);
    inventoryTable->clear();
}

void MainWindow::createInventoryWidgets()
{
    QHBoxLayout *inventoryLayout = new QHBoxLayout;
    inventoryTable = new InventoryTable(inventoryWidget, 3, 230, true);
    inventoryTable->setMaximumSize(230*3+2, 230*3+2);
    inventoryTable->setIconSize(QSize(200, 200));

    inventoryLayout->addWidget(inventoryTable);
    inventoryWidget->setLayout(inventoryLayout);

    openMainMenuButton = new QPushButton(inventoryWidget);
    openMainMenuButton->setText(tr("Главное меню"));
    connect(openMainMenuButton, SIGNAL(clicked(bool)), this, SLOT(showMainMenu()));

    QIcon stuffIcon(":/img/apple.png");
    InventoryTable *stuff = new InventoryTable(inventoryWidget,1,230, false);
    stuff->setMaximumSize(232, 232);
    stuff->setIconSize(QSize(230, 230));

    QTableWidgetItem *item = new QTableWidgetItem;
    item->setSizeHint(QSize(230, 230));
    item->setIcon(stuffIcon);
    stuff->setItem(0,0,item);

    QVBoxLayout *verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(stuff);
    verticalLayout->addWidget(openMainMenuButton);
    QSpacerItem *stuffSpacer = new QSpacerItem(100,0,QSizePolicy::Maximum,QSizePolicy::Minimum);
    QSpacerItem *bigStuffSpacer = new QSpacerItem(500,0,QSizePolicy::Maximum,QSizePolicy::Minimum);
    inventoryLayout->addSpacerItem(bigStuffSpacer);
    inventoryLayout->addLayout(verticalLayout);
    inventoryLayout->addSpacerItem(stuffSpacer);
}
