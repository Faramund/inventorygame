#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QPushButton;
class InventoryTable;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void showMainMenu();
    void hideMainMenu();
    void onStopPressed();
private:
    QWidget *backgroundWidget;
    QWidget *buttonsWidget;
    QWidget *inventoryWidget;
    QPushButton *startButton;
    QPushButton *stopButton;
    QPushButton *openMainMenuButton;
    QRect mainMenuGeometry;
    InventoryTable *inventoryTable;

    void createInventoryWidgets();
};

#endif // MAINWINDOW_H
