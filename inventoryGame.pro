#-------------------------------------------------
#
# Project created by QtCreator 2016-04-21T16:50:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = inventoryGame
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    inventorytable.cpp

HEADERS  += mainwindow.h \
    inventorytable.h

RESOURCES += \
    resources.qrc
