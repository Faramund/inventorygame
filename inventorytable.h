#ifndef INVENTORYTABLE_H
#define INVENTORYTABLE_H

#include <QTableWidget>

class InventoryTable : public QTableWidget
{
public:
    InventoryTable(QWidget *parent = 0, int countOfCells = 0, int sizeOfCells = 0, bool dropEnable = true);
private:
    int cellsSize;
    bool isDrobEnable;

    void resizeTableCells(int width, int height);
    void clearTableSelection();
    virtual void dropEvent(QDropEvent *dropEv);
    virtual void mousePressEvent(QMouseEvent *mouseEv);
    virtual void mouseReleaseEvent(QMouseEvent *mouseEv);
    virtual void dragLeaveEvent(QDragLeaveEvent *mouseEv);
};

#endif // INVENTORYTABLE_H
