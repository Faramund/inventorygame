#include "inventorytable.h"

#include <QMimeData>
#include <QDropEvent>
#include <QHeaderView>

InventoryTable::InventoryTable(QWidget *parent, int countOfCells, int sizeOfCells, bool dropEnable)
    : QTableWidget(parent)
{
    setEditTriggers(QTableWidget::NoEditTriggers);
    isDrobEnable = dropEnable;
    cellsSize = sizeOfCells;
    setColumnCount(countOfCells);
    setRowCount(countOfCells);
    verticalHeader()->setVisible(false);
    horizontalHeader()->setVisible(false);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    resizeTableCells(cellsSize, cellsSize);
    setSelectionMode(QTableWidget::SingleSelection);
    setDragDropMode(QTableWidget::DragDrop);
}

void InventoryTable::resizeTableCells(int width, int height)
{
    for (int iter = 0; iter < rowCount(); iter ++)
    {
        setRowHeight(iter, height);
        setColumnWidth(iter, width);
    }
}

void InventoryTable::clearTableSelection()
{
    selectionModel()->clear();
    clearSelection();
    clearFocus();
}

void InventoryTable::dropEvent(QDropEvent *dropEv)
{
    QTableWidgetItem * item = itemAt(dropEv->pos().x(),dropEv->pos().y());
    QTableWidgetItem *currentSelectedItem  = currentItem();
    if (!isDrobEnable)
    {
        dropEv->ignore();
        return;
    }
    if (item != NULL) {
        int count = item->text().toInt();

        if (currentSelectedItem != NULL)
            count = count + currentSelectedItem->text().toInt();
        else
            count++;

        item->setTextAlignment(Qt::AlignBottom|Qt::AlignRight);
        item->setText(QString::number(count));
        delete currentSelectedItem;
    }
    else
    {
        if (currentSelectedItem == NULL)
        {
            QTableWidget::dropEvent(dropEv);
            item = itemAt(dropEv->pos().x(),dropEv->pos().y());
            item->setTextAlignment(Qt::AlignBottom|Qt::AlignRight);
            item->setText("1");
        }
        else
        {
            QTableWidget::dropEvent(dropEv);
            delete currentSelectedItem;
        }
    }
    dropEv->accept();
    clearTableSelection();
}

void InventoryTable::mousePressEvent(QMouseEvent *mouseEv)
{
    if (mouseEv->button() == Qt::RightButton && isDrobEnable)
    {
        QTableWidgetItem * item = itemAt(mouseEv->pos().x(),mouseEv->pos().y());
        if (item == NULL)
            return;
        int count = item->text().toInt();
        count--;
        if (count <=0)
        {
            delete item;
        }
        else
        {
            item->setText(QString::number(count));
        }
        mouseEv->ignore();
        return;
    }
    if (mouseEv->button() == Qt::LeftButton && isDrobEnable)
    {
        QTableWidgetItem * item = itemAt(mouseEv->pos().x(),mouseEv->pos().y());
        if (item == NULL){
            mouseEv->ignore();
            return;
        }
    }
    QTableWidget::mousePressEvent(mouseEv);
}

void InventoryTable::mouseReleaseEvent(QMouseEvent *mouseEv)
{
    if (mouseEv->button() == Qt::LeftButton && isDrobEnable)
    {
        clearTableSelection();
        mouseEv->ignore();
        return;
    }
    QTableWidget::mouseReleaseEvent(mouseEv);
}

void InventoryTable::dragLeaveEvent(QDragLeaveEvent *mouseEv)
{
    clearTableSelection();
    QTableWidget::dragLeaveEvent(mouseEv);
}
